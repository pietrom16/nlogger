/// nLogger_test.cpp
///
/// Test code for NLogger

#include <chrono>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <fstream>
#include <ostream>
#include <sstream>
#include <string>
#include <thread>

// Use optional C++20 feature
#if __cplusplus >= 202002L
    #include <format>
    // If not available, use: https://github.com/fmtlib/fmt
#endif

// Define global minimum log level through the preprocessor:
// - Before including "nLogger.h".
// - Can be done in an include or project configuration file.
//#define NLOGGER_MIN_LOG_LEVEL debug
#include "nLogger.h"

using namespace Utils;

// Define global minimum log level through the compiler:
const LogLevel localMinLev = notice;

// - [Optional] Define a set of constants/preprocessor macros with desired local minimum log levels.
//    - These can be module/namespace/class/function/functionality/file specific.
//    - Allow to get more detailed logs in specific code areas.
//    - Define them in proper include files, in the source file where they are
//      used, or through the build system during configuration phase.
//    - Can be individually tuned according to where the interest is.
//    - These can go in the `nLogger_config.h` file.
#define NLOGGER_MIN_LOG_LEVEL_TASK_1 debug
#define NLOGGER_MIN_LOG_LEVEL_TASK_2 trace
const LogLevel localMinLev_task1 = debug;
const LogLevel localMinLev_task2 = trace;
const LogLevel localMinLev_unit_tests = info;


// Logs destination: 0 = terminal, 1 = log file
#define LOGS_OUTPUT 1

// Test structure passed to the logger as it is:
struct S {
    int i;
    float f;
    std::string s;

    S() {}

    S(int p_i, float p_f, const std::string &p_s) :
        i(p_i), f(p_f), s(p_s) {}

    std::string toStr() const {
        std::stringstream ss;
        ss << i << " " << f << " " << s;
        return ss.str();
    }
};


#if __cplusplus >= 202002L

template <>
struct std::formatter<S> : std::formatter<std::string> {
  auto format(S s, format_context& ctx) const {
    return formatter<string>::format(
      std::format("{} {} {}", s.i, s.f, s.s), ctx);
  }
};

#endif

std::ostream& operator<<(std::ostream& os, const S& s) {
    os << s.i << " " << s.f << " " << s.s;
    return os;
}

std::string& operator<<(std::string& str, const S& s) {
    std::stringstream os;
    os << s;
    str = os.str();
    return str;
}


int main()
{
    using namespace Utils;

    // Test data to log
    std::string testLogMsg = "Test log message (streams)";
    int i = 123;
    float v = 1.234f;
    S s(100, 0.01, "test");

#if LOGS_OUTPUT == 0
    NLogger log;
#else
    std::ofstream ofs("./test.log");
    NLogger log(ofs);
    //NLogger log(NLogger::AddTimestamp("./test"));
#endif

    // Set a different minimum log level at compile time
    log.SetMinLogLevel(info);

    // Set a different minimum log level at run time
    LogLevel minLevel = notice;
    log.SetMinLogLevel(minLevel);

    // Up to C++17

    log << testLogMsg << " " << i << " " << v << " " << s;
    log(trace) << testLogMsg << " " << i << " " << v << " " << s;
    log(debug) << testLogMsg << " " << i << " " << v << " " << s;
    log(info) << testLogMsg << " " << i << " " << v << " " << s;
    log(notice) << testLogMsg << " " << i << " " << v << " " << s;
    log(warn) << testLogMsg << " " << i << " " << v << " " << s;
    log(error) << testLogMsg << " " << i << " " << v << " " << s;
    log(critical) << testLogMsg << " " << i << " " << v << " " << s;
    log(fatal) << testLogMsg << " " << i << " " << v << " " << s;

    log(warn) << LOC << testLogMsg << " " << i << " " << v << " " << s;
    NLOG(warn) << testLogMsg << " - Log generated from NLOG macro. " << i << " " << v << " " << s;

    log(trace, localMinLev_unit_tests) << "Test a trace log with different local minimum log level.";
    log(debug, NLOGGER_MIN_LOG_LEVEL_TASK_2) << "Test a debug log with different local minimum log level.";
    log(info, localMinLev_task1) << "Test a info log with different local minimum log level.";
    log(notice, localMinLev_unit_tests) << "Test a notice log with different local minimum log level.";
    log(warn, localMinLev_task1) << "Test a warn log with different local minimum log level.";
    log(error, NLOGGER_MIN_LOG_LEVEL_TASK_1) << "Test a error log with different local minimum log level.";
    log(critical, localMinLev_task2) << "Test a critical log with different local minimum log level.";
    log(fatal, localMinLev_unit_tests) << "Test a fatal log with different local minimum log level.";

    // Timing tests
    using namespace std::chrono;

    const size_t nLoops = 100;
    for(size_t test = 0; test < 2; ++test)
    {
        if(test == 0)
            log.SetMinLogLevel(warn);
        else
            log.SetMinLogLevel(info);

        log(error) << "Timing <stream> test with minimum log level = " << log.GetMinLogLevelTag();

        auto beg = high_resolution_clock::now();

        for(size_t i = 1; i <= nLoops; ++i)
        {
            // Omit logging the structure for parity with <format> method, for now
            //log(notice) << "Loop n." << i << " " << testLogMsg << " " << i << " " << v << " " << s;
            log(notice) << "Loop n." << i << " " << testLogMsg << " " << i << " " << v;

            // Comment this out when testing logger performance:
            //std::this_thread::sleep_for(milliseconds(500));
        }

        auto end = high_resolution_clock::now();
        auto duration = duration_cast<nanoseconds>(end - beg);

        if(test == 0)
            log(error) << "Time with logs below threshold: " << duration.count() << " nanoseconds.";
        else
            log(error) << "Time with logs above threshold: " << duration.count() << " nanoseconds.";
    }

    log.LogStats();

    /* It looks like this approach is not feasible (without using the preprocessor):
    log<error>(testLogMsg);
    log<debug>(testLogMsg);
    log<warn> << testLogMsg << 123 << " abcd " << v;
    log<error>(LOC, testLogMsg);
    log<error>(LOCS, testLogMsg);

    Using the preprocessor:
    #define logT(LEVEL) log.Tag<LEVEL>()
    */

#if __cplusplus >= 202002L
    // From C++20

    log(warn, "Test 1 C++20 <format> log message.");
    log(error, std::format("Test {} C++20 <format> log message.", 2));
    log(critical, "Test {} C++20 <format> log message.", 3);
    log(fatal, "Test {} C++20 <format> log message {}.", 4, 5);

    std::string_view testLogNoArgs = "Test log message (format) with no arguments.";
    log(warn, testLogNoArgs);

    std::string testLogMsgF = "Test log message (format) {} {}";
    log(trace, testLogMsgF, i, v);
    log(debug, testLogMsgF, i, v);
    log(info, testLogMsgF, i, v);
    log(notice, testLogMsgF, i, v);
    log(warn, testLogMsgF, i, v);
    log(error, testLogMsgF, i, v);
    log(critical, testLogMsgF, i, v);
    log(fatal, testLogMsgF, i, v);

    log(warn, "Test structure, specifying fields {} {} {}", s.i, s.f, s.s);
    log(warn, std::format("Test structure (format), specifying fields {} {} {}", s.i, s.f, s.s));
    log(warn, std::format("Test structure (format), converting to string {}", s.toStr()));
    log(warn, std::format("Test structure (format), using formatter {}", s));
    log(warn, "Test structure (format), using formatter, with no explicit call to `std::format` {}", s);

    // Timing tests

    for(size_t test = 0; test < 2; ++test)
    {
        if(test == 0)
            log.SetMinLogLevel(warn);
        else
            log.SetMinLogLevel(info);
        log(error) << "Timing <format> test with minimum log level = " << log.GetMinLogLevelTag();

        auto beg = high_resolution_clock::now();

        for(size_t i = 1; i <= nLoops; ++i)
        {
            log(notice, "Loop n.{} {} {} {} ", i, testLogMsg, i, v);
        }

        auto end = high_resolution_clock::now();
        auto duration = duration_cast<nanoseconds>(end - beg);

        if(test == 0)
            log(error) << "Time with logs below threshold: " << duration.count() << " nanoseconds.";
        else
            log(error) << "Time with logs above threshold: " << duration.count() << " nanoseconds.";
    }

    log.LogStats();

#endif

    abort();  // to check how many logs get lost

    return 0;
}

