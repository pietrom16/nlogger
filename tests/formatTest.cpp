/// formatTest.cpp

/** Test of the C++20 <format> library for a logger.
 * 
 *    > clang++ -std=c++20 ./formatTest.cpp
 *    > g++ -std=c++20 ./formatTest.cpp
 */

/* TODO
 * - Test with the ~ equivalent {fmt} library under C++11.
 */

#include <format>
#include <iostream>

/*
class Logger {
public:

    template<typename... Args>
    Logger& operator()(const std::string& p_msg, Args... args)
    {
        const std::string msg = std::format("{} {}", p_msg, &args...);
        
        std::cerr << msg << std::endl;
        return *this;
    }
};
*/

class Logger {
public:

    template<typename... Args>
    Logger& operator()(const std::string_view& p_msg, Args&&... args)
    {
        const std::string msg = std::vformat(p_msg, std::make_format_args(std::forward<Args>(args)...));

        std::cerr << msg << std::endl;
        return *this;
    }
};

int main()
{
    Logger log;
    
    log("Test 1 <format> log message.");
    log("Test {} <format> log message.", 3);
    log("Test {} <format> log message {}.", 4, 5);

    return 0;
}

/*
Error messages:

error: call to consteval function 'std::basic_format_string<char, const std::basic_string<char> &>::basic_format_string<char[6]>' is not a constant expression
        const std::string msg = std::format("{} {}", p_msg, &args...);
                                            ^
./formatTest.cpp:31:8: note: in instantiation of function template specialization 'Logger::operator()<>' requested here
    log("Test 1 <format> log message.");
       ^
*/


//    log(std::format("Test {} <format> log message.", 2));
//    log(std::format("Test {} <format> log message {}.", 4, 5));

        
        
