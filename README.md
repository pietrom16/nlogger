# nLogger

**nLogger** is *yet* another logger, with the following features:

- Allows to set minimum log level thresholds in multiple ways:
    - Statically or dynamically.
    - Per code block, function, class, namespace, file, functionality.
    - Allows to have detailed logs from the desired code areas, only.
- No use of the preprocessor. Unneded code removed by the compiler.
- High performance.
    - Suited to single and multithreading processing.
    - For multiple threads, define the NLOGGER_MULTI_THREADING preprocessor macro.
- A single header and a single source file (which can be merged to get a header-only logger).
- Can use both `<fstream>` and C++20 `<format>`.
- Open Source (Apache 2.0).
- Requirements: Standard C++11, optional: Standard C++20.
- Log message format:
      `Timestamp LogLevel [SourceLocation - ] LogMessage`

## Installation

No installation required. Just copy this logger sources in your project and adjust 
your project configuration files.

## Usage

This example show basic usage. For more details, including performance tests, 
look at the `nLogger_test.cpp` file.

```cpp
// A global minimum log level can be set from the build system with this macro:
#define NLOGGER_MIN_LOG_LEVEL debug
#include "nLogger.h"

// [Optional] Define a set of constants/preprocessor macros with desired local minimum log levels.
//  - These can be module/namespace/class/function/functionality/file specific.
//  - Allow to get more detailed logs in specific code areas.
//  - Define them in proper include files, in the source file where they are
//    used, or through the build system during configuration phase.
//  - Can be individually tuned according to where the interest is.
#define NLOGGER_MIN_LOG_LEVEL_TASK_1 debug
#define NLOGGER_MIN_LOG_LEVEL_TASK_2 trace
const LogLevel localMinLev_task1 = info;
const LogLevel localMinLev_task2 = trace;
const LogLevel localMinLev_unit_tests = debug;


// Structures/classes can be logged...
struct S {
    S(int p_i, float p_f, const std::string& p_s) :
        i(p_i), f(p_f), s(p_s) {}
    int i;
    float f;
    std::string s;
};

// ...just provide their insertion operator:
std::ostream& operator<<(std::ostream& os, const S& s) {
    os << s.i << " " << s.f << " " << s.s;
    return os;
}

void TestFunc();

int main()
{
    using namespace Utils;

    // Test data to log
    std::string testLogMsg = "Test log message";
    int i = 123;
    float v = 1.234f;
    S s(100, 0.01, "test");

    std::ofstream ofs("./test.log");
    NLogger log(ofs);

    // Set a different minimum log level at compile time
    log.SetMinLogLevel(notice);
    
    // Set a different minimum log level at run time
    LogLevel minLevel = notice;
    log.SetMinLogLevel(minLevel);
    
    log(warn) << testLogMsg << " " << i << " " << v << " " << s;
    
    TestFunc();
    
    abort();  // simulate a crash, to check whether the previous log is lost
}

void TestFunc()
{
    // This log will be generated even if at `info` level, i.e. below the global `notice` level:
    log(info, localMinLev_unit_tests) << "Running a test...";
}
```

### Performance improvement trick

To substantially reduce the performance impact of nLogger, it is suggested 
to create the log file on a RAM-disk, which will be periodically backed up
on a long term memory. If the program crashes, the log file will be safe 
on the RAM-disk. If the system crashes, the log file will be lost.

This approach also greatly reduces the number of write operations on disk/SSD,
reducing the device's wearing and prolonging its operational life.

## Performance tests

Configuration:
- Test platform: x86_64-pc-linux-gnu, Ryzen 7 2700, 24GB RAM
- Compiler: C++20, clang 16.0.6.
- Compiler settings: optimized release mode.
- Single thread.
- 1000 logs saved on RAM-disk:

    | Test type                      | `<stream>`  | `<format>` |
    |--------------------------------|------------:|-----------:|
    | Time with logs below threshold |     9010 ns |    3352 ns |
    | Time with logs above threshold |  7114452 ns | 7293176 ns |



## Roadmap

- Check generated assembly code with logs enabled and disabled.
- Add utility to backup the current log file when above a certain size.

## Name origin...

The **n** in front of *Logger* is there for historic reasons: it was initially 
developed as a logger for a neural network project.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Authors and acknowledgment

The author of this project is [Pietro Mele](https://gitlab.com/pietrom16).

## License

[Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt)




