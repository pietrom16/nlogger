/// nLogger_config.h

/** nLogger configuration file
 *
 *  Note: these preprocessor macros can be defined through the build system.
 */

#ifndef NLOGGER_CONFIG_H
#define NLOGGER_CONFIG_H

// Multi-threading environment: comment out if it does not apply
//#define NLOGGER_MULTI_THREADING
//#define NLOGGER_LOG_THREAD_ID

// Multi-processing environment: comment out if it does not apply
// Requirements: Boost version >= 1.82.0
//#define NLOGGER_MULTI_PROCESSING

// Minimum log level below which the logs are excluded from the build
#define NLOGGER_MIN_LOG_LEVEL notice

// Default log files destination directory
#define NLOGGER_DEFAULT_FILEPATH "."

#endif // NLOGGER_CONFIG_H
