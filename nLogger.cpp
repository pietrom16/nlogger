/// nLogger.cpp

/* Copyright 2024 Pietro Mele
 *
 * License: Apache License, Version 2.0
 */

#include "nLogger.h"
#include <format>
#include <sstream>

#if __cplusplus < 202002L
#include <iomanip>
#endif

namespace Utils {

LogStats::LogStats() {
    Reset();
}

void LogStats::Reset() {
    nLogs = 0;
    nErrorLogs = 0;
    highestLogLevel = trace;
    dateLastReset = NLogger::LogTimeStamp();
    timeSinceStart = std::chrono::system_clock::now();
}

#if __cplusplus < 201703L
char const NLogger::c_logLevelTag[NLogger::c_nLogLevels][NLogger::c_logLevelTagSize] =
    {
        "trace    ", "debug    ", "info     ", "notice   ",
        "warn     ", "ERROR    ", "CRITICAL ", "FATAL    "
    };
#endif

const std::string NLogger::c_filenameExt = ".log";
struct LogStats NLogger::s_stats;

std::string NLogger::AddTimestamp(const std::string& p_filePath)
{
    //TODO - Check if already timestamped

    // Read filename extension
    const auto dotPos = p_filePath.find_last_of(".");
    std::string filenameExt;
    if(dotPos != std::string::npos) {
        filenameExt = p_filePath.substr(dotPos);
    }
    else {
        filenameExt = c_filenameExt;
    }

    //TODO - Cut the filename extension, if present

    std::string filePath(p_filePath);
    filePath.append("_");
    filePath.append(TimeStamp());
    filePath.append(filenameExt);
    return filePath;
}

std::string NLogger::TimeStamp()
{
#if __cplusplus >= 202002L
    const auto now = std::chrono::time_point_cast<std::chrono::seconds>(
        std::chrono::system_clock::now()
        );
    const std::string timeStamp = std::format("{:%Y%m%d-%H%M%S}", now).c_str();
#else
    const auto now = std::chrono::system_clock::now();
    const auto now_c = std::chrono::system_clock::to_time_t(now);

    char buf[32] = {0};
    std::strftime(buf, sizeof(buf), "%Y%m%d-%H%M%S", std::localtime(&now_c));
    const std::string timeStamp = buf;
#endif

    return timeStamp;
}

std::string NLogger::LogTimeStamp()
{
#if __cplusplus >= 202002L
    const auto now = std::chrono::time_point_cast<std::chrono::milliseconds>(
        std::chrono::system_clock::now()
        );

    const std::string timeStamp = std::format("{0:%F}T{0:%T%Z}", now);  // UTC format, with 'UTC' extension
    //const std::string timeStamp = std::format("{0:%F}T{0:%T%z}", now);  // UTC format, with '+0000' extension

#else
    // 2024-04-09T12:32:38.388UTC

    using namespace std::chrono;

    const auto now = system_clock::now();

    // Number of milliseconds for the current second
    const auto ms = duration_cast<milliseconds>(now.time_since_epoch()) % 1000;

    const auto currentTime = system_clock::to_time_t(now);
    //const std::tm bt = *std::localtime(&currentTime);
    const std::tm bt = *std::gmtime(&currentTime);

    std::ostringstream oss;

    oss << std::put_time(&bt, "%Y-%m-%dT%H:%M:%S");
    oss << '.' << std::setfill('0') << std::setw(3) << ms.count();
    oss << "UTC";

    const std::string timeStamp = oss.str();
#endif

    return timeStamp;
}

int NLogger::Init() {
    ResetLogStats();
    //TODO
    return 0;
}

LogLevel NLogger::SetMinLogLevel(LogLevel p_minLevel) {
    operator()(info) << "Change minimum log level from " << c_logLevelTag[minLogLevel] << " to " << c_logLevelTag[p_minLevel];
    minLogLevel = p_minLevel;
    return minLogLevel;
}

void NLogger::LogStats(const LogLevel p_level) {

    using namespace std::chrono;

    if(p_level < c_minLogLevel) return;
    if(p_level < minLogLevel) return;

    NLOGGER_MUTEX;

    const auto now = system_clock::now();
    const auto elapsedTime = duration_cast<milliseconds>(now - stats.timeSinceStart);

    osLog << LogTimeStamp()
          << "   Log statistics for logger " << loggerName << ":\n"
          << "      Time interval: [" << stats.dateLastReset << " , " << LogTimeStamp() << "], duration: " << elapsedTime.count() << " ms\n"
          << "      Number of logs: " << stats.nLogs << "\n"
          << "      Number of critical logs: " << stats.nErrorLogs << "\n"
          << "      Highest level log: " << NLogger::GetLogLevelTag(stats.highestLogLevel) << std::endl;

    ++stats.nLogs;
    ++s_stats.nLogs;
}

void NLogger::ResetLogStats() {
    stats.Reset();
}

void NLogger::ResetGlobalLogStats() {
    s_stats.Reset();
}


} // Utils
