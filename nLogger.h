/// nLogger.h

/* Copyright 2024 Pietro Mele
 *
 * License: Apache License, Version 2.0
 */

#ifndef NLOGGER_H
#define NLOGGER_H

#define NLOGGER_VERSION "4.2.0"

#include "nLogger_config.h"

#include <chrono>
#include <ctime>
#include <fstream>
#include <iostream>
#include <ostream>
#include <string>

#if defined(NLOGGER_MULTI_THREADING) || defined(NLOGGER_LOG_THREAD_ID)
#include <thread>
#endif

#ifdef NLOGGER_MULTI_PROCESSING
#include <boost/process/v2/pid.hpp>
#endif

#if __cplusplus >= 202002L
    #include <format>
    // If not available, use: https://github.com/fmtlib/fmt
#endif

#ifndef NLOGGER_MIN_LOG_LEVEL
#define NLOGGER_MIN_LOG_LEVEL notice
#endif

#ifndef NLOGGER_DEFAULT_FILEPATH
#define NLOGGER_DEFAULT_FILEPATH "."
#endif

#ifdef NLOGGER_MULTI_THREADING
#define NLOGGER_MUTEX std::mutex logMutex; std::lock_guard<std::mutex> lock(logMutex);
#else
#define NLOGGER_MUTEX ;
#endif

#ifndef __FILE_NAME__
// This only misses in Windows, so backslashes are used
#define __FILE_NAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#endif

#define LOC SourceLocation(__FILE_NAME__, __func__, __LINE__)
#define NLOG(level) log(level) << LOC


namespace Utils {

/// Log's severity

enum LogLevel {
    trace,      /* trace the path of code execution                              */
    debug,      /* detailed information for debugging                            */
    info,       /* information about normal operation                            */
    notice,     /* normal but significant condition that may require monitoring  */
    warn,       /* potential issue that may lead to subsequent errors            */
    error,      /* error condition that can cause wrong behaviour                */
    critical,   /* condition that demands intervention to prevent system failure */
    fatal,      /* the system is not working and requires immediate attention    */
};

class NLogger;
class ProxyLogger;

/// Log position in the source code

class SourceLocation
{
    const char *fileName = 0;
    const char *function = 0;
    size_t      lineNumber = 0;

    friend NLogger& operator<<(NLogger& p_logger, const SourceLocation& p_sl);

public:
    SourceLocation(const char* p_fileName, const char* p_function, size_t p_lineNumber)
        : fileName(p_fileName), function(p_function), lineNumber(p_lineNumber)
    {}
};

/// Logger statistics

struct LogStats
{
    std::string dateLastReset;                 // date of last reset
    std::chrono::time_point<std::chrono::system_clock>
                timeSinceStart;                // time since program start
    size_t      nLogs = 0;                     // number of logs produced
    size_t      nErrorLogs = 0;                // number of logs with log level above or equal to error
    LogLevel    highestLogLevel = trace;       // highest log level during a run

    LogStats();
    void Reset();
};

/// Main logger class

class NLogger
{
    friend class ProxyLogger;

    template<typename T>
    friend ProxyLogger operator<<(NLogger& p_logger, const T& p_token);

    friend NLogger& operator<<(NLogger& p_logger, const SourceLocation& p_sl);

    static const size_t c_nLogLevels = fatal + 1;
    static const size_t c_logLevelTagSize = 10;
#if __cplusplus >= 201703L
    static constexpr char const c_logLevelTag[c_nLogLevels][c_logLevelTagSize] =
    {
        "trace    ", "debug    ", "info     ", "notice   ",
        "warn     ", "ERROR    ", "CRITICAL ", "FATAL    "
    };
#else
    static char const c_logLevelTag[c_nLogLevels][c_logLevelTagSize];
#endif

public:
    NLogger() : osLog(std::cerr) {}

    explicit NLogger(std::ostream& p_ofs) : osLog(p_ofs) {}

    explicit NLogger(const std::string& p_filePath) :
        filePath(p_filePath),
        ofsLog(p_filePath),
        osLog(ofsLog)
    {}

    //TODO NLogger(const std::string& p_filePath, const std::string& p_configFile);

    NLogger(const NLogger&) = delete;
    NLogger& operator=(const NLogger&) = delete;
    ~NLogger() = default;

    // Stream based implementation
    NLogger& operator()(const LogLevel p_level) {
        logLevel = p_level;
        if(logLevel < c_minLogLevel) return *this;
        if(logLevel < minLogLevel) return *this;
        NLOGGER_MUTEX;
        osLog << LogTimeStamp() << " " << c_logLevelTag[logLevel] << " ";
#ifdef NLOGGER_MULTI_PROCESSING
        osLog << "pid=\"" << boost::process::v2::current_pid() << "\" - ";
#endif
#ifdef NLOGGER_LOG_THREAD_ID
        osLog << "tid=\"" << std::this_thread::get_id() << "\" - ";
#endif
        ++stats.nLogs;
        ++s_stats.nLogs;
        if(logLevel >= error) {
            ++stats.nErrorLogs;
            ++s_stats.nErrorLogs;
        }
        if(logLevel > stats.highestLogLevel) stats.highestLogLevel = logLevel;
        if(logLevel > s_stats.highestLogLevel) s_stats.highestLogLevel = logLevel;
        return *this;
    }

    NLogger& operator()(const LogLevel p_level, const LogLevel p_localMinLogLevel) {
        logLevel = p_level;
        localMinLogLevel = p_localMinLogLevel;
        if(p_level < c_minLogLevel) return *this;
        if(p_level < minLogLevel && p_level < p_localMinLogLevel) return *this;
        NLOGGER_MUTEX;
        osLog << LogTimeStamp() << " " << c_logLevelTag[logLevel] << " ";
#ifdef NLOGGER_MULTI_PROCESSING
        osLog << "pid=\"" << boost::process::v2::current_pid() << "\" - ";
#endif
#ifdef NLOGGER_LOG_THREAD_ID
        osLog << "tid=\"" << std::this_thread::get_id() << "\" - ";
#endif
        ++stats.nLogs;
        ++s_stats.nLogs;
        if(logLevel >= error) {
            ++stats.nErrorLogs;
            ++s_stats.nErrorLogs;
        }
        if(logLevel > stats.highestLogLevel) stats.highestLogLevel = logLevel;
        if(logLevel > s_stats.highestLogLevel) s_stats.highestLogLevel = logLevel;
        return *this;
    }

    // <format> based implementation
#if __cplusplus >= 202002L
    template<typename... Args>
    NLogger& operator()(const LogLevel p_level, const std::string_view& p_msg, Args&&... p_args) {
        logLevel = p_level;
        if(logLevel < c_minLogLevel) return *this;
        if(logLevel < minLogLevel) return *this;
        NLOGGER_MUTEX;
        const std::string dateLevel = std::format("{} {}", LogTimeStamp(), c_logLevelTag[logLevel]);
        const std::string msg = std::vformat(p_msg, std::make_format_args(std::forward<Args>(p_args)...));
        osLog << dateLevel << " ";
#ifdef NLOGGER_MULTI_PROCESSING
        osLog << "pid=\"" << boost::process::v2::current_pid() << "\" - ";
#endif
#ifdef NLOGGER_LOG_THREAD_ID
        osLog << "tid=\"" << std::this_thread::get_id() << "\" - ";
#endif
        osLog << msg << std::endl;

        ++stats.nLogs;
        ++s_stats.nLogs;
        if(logLevel >= error) {
            ++stats.nErrorLogs;
            ++s_stats.nErrorLogs;
        }
        if(logLevel > stats.highestLogLevel) stats.highestLogLevel = logLevel;
        if(logLevel > s_stats.highestLogLevel) s_stats.highestLogLevel = logLevel;
        return *this;
    }
#endif

    static std::string AddTimestamp(const std::string& p_filePath);

    LogLevel SetMinLogLevel(LogLevel p_minLevel);
    LogLevel GetMinLogLevel() const { return minLogLevel; }
    std::string GetMinLogLevelTag() const { return c_logLevelTag[minLogLevel]; }
    static std::string GetLogLevelTag(LogLevel p_level) { return c_logLevelTag[p_level]; }
    static size_t GetNLogLevels() { return c_nLogLevels; }
    void LogStats(const LogLevel p_level = info);
    void ResetLogStats();
    static void ResetGlobalLogStats();
    static std::string TimeStamp();
    static std::string LogTimeStamp();

private:
    int Init();

private:
    std::string loggerName;

    std::ofstream ofsLog;
    std::ostream& osLog;

    const std::string c_filePathDefault = NLOGGER_DEFAULT_FILEPATH;
    static const std::string c_filenameExt;
    std::string filePath = c_filePathDefault;

    const LogLevel c_minLogLevel = NLOGGER_MIN_LOG_LEVEL,
                   c_maxLogLevel = fatal;
    LogLevel logLevel = c_minLogLevel;
    LogLevel minLogLevel = c_minLogLevel;
    LogLevel localMinLogLevel = c_maxLogLevel;

    struct LogStats stats;           // this logger statistics
    static struct LogStats s_stats;  // all loggers statistics

#ifdef NLOGGER_MULTI_THREADING
    std::mutex logMutex;
#endif
};


inline NLogger& operator<<(NLogger& p_logger, const SourceLocation& p_sl)
{
    if(p_logger.logLevel < p_logger.c_minLogLevel) return p_logger;
    if(p_logger.logLevel < p_logger.minLogLevel && p_logger.logLevel < p_logger.localMinLogLevel) return p_logger;
    NLOGGER_MUTEX;
    p_logger.osLog << p_sl.fileName << ":" << p_sl.function << ":" << p_sl.lineNumber << " - ";
    return p_logger;
}


/** ProxyLogger
 * Auxiliary class to flush logs without the need of a terminator (endl, flush, ...)
 */

class ProxyLogger
{
    NLogger &logger;

public:
    explicit ProxyLogger(NLogger& p_logger) : logger(p_logger) {}

    ~ProxyLogger() {
        if(logger.logLevel >= logger.c_minLogLevel) {
            if(logger.logLevel >= logger.minLogLevel || logger.logLevel >= logger.localMinLogLevel) {
                logger.osLog << std::endl;
                logger.localMinLogLevel = logger.c_maxLogLevel;
            }
        }
    }

    template<typename T>
    ProxyLogger& operator<<(const T& p_token) {
        if(logger.logLevel < logger.c_minLogLevel) return *this;
        if(logger.logLevel < logger.minLogLevel && logger.logLevel < logger.localMinLogLevel) return *this;
        NLOGGER_MUTEX;
        logger.osLog << p_token;
        return *this;
    }
};

template<typename T>
ProxyLogger operator<<(NLogger& p_logger, const T& p_token) {
    if(p_logger.logLevel < p_logger.c_minLogLevel) return ProxyLogger(p_logger);
    if(p_logger.logLevel < p_logger.minLogLevel && p_logger.logLevel < p_logger.localMinLogLevel) return ProxyLogger(p_logger);
    NLOGGER_MUTEX;
    p_logger.osLog << p_token;
    return ProxyLogger(p_logger);
}

} // Utils

#endif // NLOGGER_H


